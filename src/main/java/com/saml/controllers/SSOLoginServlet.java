package com.saml.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.opensaml.saml2.core.AuthnRequest;

import com.saml.operations.AuthRequestParser;

@WebServlet("/saml/SSO")
public class SSOLoginServlet extends HttpServlet {
	static Logger logger = Logger.getLogger(SSOLoginServlet.class.getName());
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws ServletException, IOException {
		PrintWriter pw = httpServletResponse.getWriter();
		pw.print("Please wait...");
		
		ssoLogin(httpServletRequest,httpServletResponse);
		
		pw.close();
	}

	protected void ssoLogin(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		// Parse request
		AuthRequestParser authRequestParser = new AuthRequestParser();
		AuthnRequest authnRequest = authRequestParser.parseAuthRequest(httpServletRequest);

		if (authnRequest == null)
			return;
		
		//check for session
		logger.info("checking for existing session..");
		HttpSession session = httpServletRequest.getSession();
		if (session == null) {
			logger.info("No session found");
			return;
		}
		
		//adding authnRequest and relayState to session
		session.setAttribute("authnRequest", authnRequest);
		session.setAttribute("relayState", httpServletRequest.getParameter("RelayState"));

		//checking existence of userProfile in session
		if (session.getAttribute("userProfile") == null) {
			// forward control to OIDC
			
			logger.info("No existing session found.");
			logger.info("transfering control to Relying party");
			RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/authRequester");
			try {
				dispatcher.forward(httpServletRequest, httpServletResponse);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			logger.info("Existing session found with id : "+ session.getId());
			logger.info("transfering control to assertion builder");
			
			RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/saml/asserter");
			try {
				dispatcher.forward(httpServletRequest, httpServletResponse);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return;
	}
}
