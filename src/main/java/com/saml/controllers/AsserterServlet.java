package com.saml.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.opensaml.saml2.core.AuthnRequest;

import com.saml.operations.SAMLResponseBuilder;
import com.saml.operations.Utils;

@WebServlet("/saml/asserter")
public class AsserterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(AsserterServlet.class.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws ServletException, IOException {
		PrintWriter pw = null;
		try {
			pw = httpServletResponse.getWriter();

			HttpSession session = checkForSession(httpServletRequest);

			if (session != null) {
				AuthnRequest authnRequest = (AuthnRequest) session.getAttribute("authnRequest");
				String relayState = (String) session.getAttribute("relayState");
				HashMap<String, String> userProfile = (HashMap<String, String>) session.getAttribute("userProfile");

				SAMLResponseBuilder samlResponseBuilder = new SAMLResponseBuilder();
				String samlResponse = samlResponseBuilder.buildSAMLResponse(authnRequest, userProfile);

				String encodedSAMLRespons = Utils.encodeSaml(samlResponse);

				try {

					httpServletResponse.getWriter().write(Utils.getPostMessage(encodedSAMLRespons, authnRequest.getAssertionConsumerServiceURL(), relayState));

				} catch (IOException e) {
					logger.error("File Not Found");
					logger.error(e.toString());
					e.printStackTrace();
				}

			} else {
				pw.print("Internal error...");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		pw.close();

	}

	private HttpSession checkForSession(HttpServletRequest httpServletRequest) {
		// TODO Auto-generated method stub
		logger.info("checking for existing session..");
		HttpSession session = httpServletRequest.getSession();
		if (session == null) {
			logger.info("No session found");
			return null;
		}
		logger.info("Session found with id:" + session.getId());
		return session;
	}

}
