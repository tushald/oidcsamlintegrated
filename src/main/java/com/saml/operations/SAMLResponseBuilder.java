package com.saml.operations;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.core.impl.ResponseBuilder;
import org.opensaml.saml2.core.impl.ResponseMarshaller;
import org.opensaml.saml2.core.impl.StatusBuilder;
import org.opensaml.saml2.core.impl.StatusCodeBuilder;
import org.opensaml.xml.util.XMLHelper;
import org.w3c.dom.Element;

public class SAMLResponseBuilder {
	static Logger logger = Logger.getLogger(SAMLResponseBuilder.class.getName());
	private Properties properties = null;

	public String buildSAMLResponse(AuthnRequest authnRequest, Map<String, String> userProfile) {
		logger.info("Building SAML response");

		try {
			// loading properties file
			properties = getProperties();
			if (properties == null)
				return null;

			Integer samlAssertionExpirationMin = Integer.parseInt(properties.getProperty("samlAssertionExpirationMin"));

			// fetching info for assertion
			String issuer = properties.getProperty("issuer");
			String subject = userProfile.get(properties.getProperty("username"));

			HashMap<String, List<String>> attributes = getUserAttribute(userProfile);

			Status status = createStatus();
			Issuer responseIssuer = null;
			Issuer assertionIssuer = null;

			String nameIdFormat = authnRequest.getNameIDPolicy() != null ? authnRequest.getNameIDPolicy().getFormat()
					: null;
			if (issuer != null) {
				responseIssuer = createIssuer(issuer, nameIdFormat);
				assertionIssuer = createIssuer(issuer, nameIdFormat);
			}

			// building SAML assertion
			SamlAssertionProducer producer = new SamlAssertionProducer();
			Assertion assertion = (Assertion) producer.buildAssertion(subject, new DateTime(),
					properties.getProperty("password"), attributes, assertionIssuer, samlAssertionExpirationMin,
					authnRequest);

			// creating response
			Response response = createResponse(new DateTime(), responseIssuer, status, assertion, authnRequest);

			// signing response
			String urlForPrivateKeyFile = getClass().getResource(properties.getProperty("privateKeyLocation"))
					.getPath();
			String urlForCertFile = getClass().getResource(properties.getProperty("certificateLocation")).getPath();

			ResponseSigner responseSigner = new ResponseSigner();
			responseSigner.setPrivateKeyLocation(urlForPrivateKeyFile);
			responseSigner.setPublicKeyLocation(urlForCertFile);
			Response signedResponse = responseSigner.signResponse(assertion, response);

			ResponseMarshaller marshaller = new ResponseMarshaller();
			Element element = marshaller.marshall(signedResponse);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			XMLHelper.writeNode(element, baos);

			String responseStr = new String(baos.toByteArray());
			logger.debug("SAML response\n" + responseStr);
			logger.info("Successfully created and signed SAML response");
			return responseStr;

		} catch (Throwable t) {
			t.printStackTrace();
		}
		return null;
	}

	private HashMap<String, List<String>> getUserAttribute(Map<String, String> userProfile) {
		
		HashMap<String, List<String>> attributes = new HashMap<String, List<String>>();
		List<String> uname1 = Arrays.asList(userProfile.get("given_name") +" " + userProfile.get("family_name"));
		
		attributes.put(properties.getProperty("name"),uname1);
		attributes.put(properties.getProperty("email"), Arrays.asList(userProfile.get("email")));
		
		return attributes;
	}

	private Properties getProperties() {
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream reader = classLoader.getResourceAsStream("/application.properties");
			Properties properties = new Properties();
			properties.load(reader);
			return properties;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// response creator
	private Response createResponse(final DateTime issueDate, Issuer issuer, Status status, Assertion assertion,
			AuthnRequest authnRequest) {
		ResponseBuilder responseBuilder = new ResponseBuilder();
		Response response = responseBuilder.buildObject();
		response.setInResponseTo(authnRequest.getID());
		response.setDestination(authnRequest.getAssertionConsumerServiceURL());
		response.setIssueInstant(issueDate);
		response.setVersion(SAMLVersion.VERSION_20);
		response.setID(UUID.randomUUID().toString());

		System.out.println(authnRequest.getAssertionConsumerServiceURL());
		System.out.println(authnRequest.getID());
		response.setIssuer(issuer);
		response.setStatus(status);
		response.getAssertions().add(assertion);
		return response;
	}

	// issuer object creator
	private Issuer createIssuer(final String issuerName, String nameIdFormat) {
		IssuerBuilder issuerBuilder = new IssuerBuilder();
		Issuer issuer = issuerBuilder.buildObject();
		issuer.setFormat(properties.getProperty("issuerNameIdFormat"));
		issuer.setValue(issuerName);
		logger.info("Response issuer object is created");
		return issuer;
	}

	// status object creator
	private Status createStatus() {
		StatusCodeBuilder statusCodeBuilder = new StatusCodeBuilder();
		StatusCode statusCode = statusCodeBuilder.buildObject();
		statusCode.setValue(StatusCode.SUCCESS_URI);

		StatusBuilder statusBuilder = new StatusBuilder();
		Status status = statusBuilder.buildObject();
		status.setStatusCode(statusCode);

		logger.info("Response status object is created");
		return status;
	}
}
