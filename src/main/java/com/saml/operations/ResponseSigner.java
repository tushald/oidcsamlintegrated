package com.saml.operations;

import java.io.ByteArrayOutputStream;

import org.apache.log4j.Logger;
import org.opensaml.Configuration;
import org.opensaml.common.impl.SAMLObjectContentReference;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.impl.ResponseMarshaller;
import org.opensaml.xml.encryption.EncryptionConstants;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureConstants;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.util.XMLHelper;
import org.w3c.dom.Element;

public class ResponseSigner {
	static Logger logger = Logger.getLogger(ResponseSigner.class.getName());
	private String privateKeyLocation;
	private String publicKeyLocation;
	private CertManager certManager = new CertManager();

	public Response signResponse(Assertion assertion,Response response) throws Throwable {
		logger.info("Signing response");
		
		Signature signature = createSignature();
		
		assertion.setSignature(signature);

		((SAMLObjectContentReference) signature.getContentReferences().get(0))
				.setDigestAlgorithm(EncryptionConstants.ALGO_ID_DIGEST_SHA256);

		ResponseMarshaller marshaller = new ResponseMarshaller();
		Element element = marshaller.marshall(response);

		if (signature != null) {
			Signer.signObject(signature);
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLHelper.writeNode(element, baos);
		
		logger.info("Successfully signed response");
		
		return response;

	}

	public String getPrivateKeyLocation() {
		return privateKeyLocation;
	}

	public void setPrivateKeyLocation(String privateKeyLocation) {
		this.privateKeyLocation = privateKeyLocation;
	}

	public String getPublicKeyLocation() {
		return publicKeyLocation;
	}

	public void setPublicKeyLocation(String publicKeyLocation) {
		this.publicKeyLocation = publicKeyLocation;
	}

	public Signature createSignature() throws Throwable {
		if (publicKeyLocation != null && privateKeyLocation != null) {
			
			Signature signature = (Signature) Configuration.getBuilderFactory()
					.getBuilder(Signature.DEFAULT_ELEMENT_NAME).buildObject(Signature.DEFAULT_ELEMENT_NAME);
			signature.setSigningCredential(certManager.getSigningCredential(publicKeyLocation, privateKeyLocation));
			signature.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256);
			signature.setCanonicalizationAlgorithm(SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
			SecurityHelper.prepareSignatureParams(signature, signature.getSigningCredential(), null, null);

			return signature;
		}
		return null;
	}
}
