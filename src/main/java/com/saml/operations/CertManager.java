package com.saml.operations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.x509.BasicX509Credential;

public class CertManager {
	static Logger logger = Logger.getLogger(CertManager.class.getName());
	
	
	public Credential getSigningCredential(String publicKeyLocation, String privateKeyLocation) throws Throwable {
		logger.info("Getting signing credential");
		// create public key (cert) portion of credential
		InputStream inStream = new FileInputStream(publicKeyLocation);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate cert = (X509Certificate)cf.generateCertificate(inStream);
		inStream.close();
		    
		PrivateKey privateKey =getPrivateKey(privateKeyLocation);
		// create credential and initialize
		BasicX509Credential credential = new BasicX509Credential();
		credential.setEntityCertificate(cert);
		credential.setPrivateKey(privateKey);
		credential.setEntityCertificate(cert);
		logger.info("Successfully got credentials");
		return credential;
	}
	
	private static String getKey(String filename) throws IOException {
		// Read key from file
		String strKeyPEM = "";
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		while ((line = br.readLine()) != null) {
			strKeyPEM += line + "\n";
		}
		br.close();
		return strKeyPEM;
	}

	public static PrivateKey getPrivateKey(String filename) throws IOException, GeneralSecurityException {
		
		String privateKeyPEM = getKey(filename);
		return getPrivateKeyFromString(privateKeyPEM);
	}
	
	public static PrivateKey getPrivateKeyFromString(String key) throws IOException, GeneralSecurityException {
	    
		String privateKeyPEM = key;
	    
	    privateKeyPEM = privateKeyPEM.replace("-----BEGIN RSA PRIVATE KEY-----", "");
	    privateKeyPEM = privateKeyPEM.replace("-----END RSA PRIVATE KEY-----","");
	    
	    byte[] encoded = Base64.decodeBase64(privateKeyPEM.getBytes());
	    KeyFactory kf = KeyFactory.getInstance("RSA");
			
	    ASN1EncodableVector v = new ASN1EncodableVector();
	    v.add(new ASN1Integer(0));
	    ASN1EncodableVector v2 = new ASN1EncodableVector();
	    v2.add(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId()));
	    v2.add(DERNull.INSTANCE);
	    v.add(new DERSequence(v2));
	    v.add(new DEROctetString(encoded));
	    ASN1Sequence seq = new DERSequence(v);
	    byte[] privKeybytes = seq.getEncoded("DER");
	    
	    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privKeybytes);
	    PrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpec);
	    return privKey;
	}

}
