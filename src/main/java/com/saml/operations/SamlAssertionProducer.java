package com.saml.operations;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.ws.security.saml.ext.bean.AudienceRestrictionBean;
import org.apache.ws.security.saml.ext.bean.ConditionsBean;
import org.apache.ws.security.saml.ext.builder.SAML2ComponentBuilder;
import org.joda.time.DateTime;
import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.impl.AssertionBuilder;
import org.opensaml.saml2.core.impl.AttributeBuilder;
import org.opensaml.saml2.core.impl.AttributeStatementBuilder;
import org.opensaml.saml2.core.impl.AuthnContextBuilder;
import org.opensaml.saml2.core.impl.AuthnContextClassRefBuilder;
import org.opensaml.saml2.core.impl.AuthnStatementBuilder;
import org.opensaml.saml2.core.impl.NameIDBuilder;
import org.opensaml.saml2.core.impl.SubjectBuilder;
import org.opensaml.saml2.core.impl.SubjectConfirmationBuilder;
import org.opensaml.saml2.core.impl.SubjectConfirmationDataBuilder;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.schema.impl.XSStringBuilder;

public class SamlAssertionProducer {
	static Logger logger = Logger.getLogger(SamlAssertionProducer.class.getName());
	private Properties properties = null;

	public Assertion buildAssertion(final String subjectId, final DateTime authenticationTime,
			final String credentialType, final HashMap<String, List<String>> attributes, Issuer assertionIssuer,
			Integer samlAssertionExpirationMin, AuthnRequest authnRequest) {
			logger.info("building assertion for userProfile");
			
		try {
			// loading properties file
			properties = getProperties();
			if (properties == null)
				return null;
			
			
			DefaultBootstrap.bootstrap();
			
			//fetching info
			Subject subject = null;
			AttributeStatement attributeStatement = null;
			String nameIdFormat =authnRequest.getNameIDPolicy()!=null? authnRequest.getNameIDPolicy().getFormat():null;

			if (subjectId != null) {
				subject = createSubject(subjectId, samlAssertionExpirationMin,nameIdFormat,authnRequest.getAssertionConsumerServiceURL());
			}

			if (attributes != null && attributes.size() != 0) {
				attributeStatement = createAttributeStatement(attributes);
			}

			
			//building authnStatement
			AuthnStatement authnStatement = createAuthnStatement(authenticationTime);

			//creating assertion
			Assertion assertion = createAssertion(new DateTime(), subject, assertionIssuer, authnStatement,
					attributeStatement, authnRequest.getAssertionConsumerServiceURL(), authnRequest.getIssuer().getValue());
			logger.info("Successfully created assertion");
			return assertion;

		} catch (Throwable t) {
			t.printStackTrace();
			return null;
		}
	}


	private Assertion createAssertion(final DateTime issueDate, Subject subject, Issuer issuer,
		AuthnStatement authnStatement, AttributeStatement attributeStatement, String asertionConsumerURL, String requestIssuer) {
		AssertionBuilder assertionBuilder = new AssertionBuilder();
		Assertion assertion = assertionBuilder.buildObject();
		assertion.setID(UUID.randomUUID().toString());
		assertion.setIssueInstant(issueDate);
		assertion.setSubject(subject);
		assertion.setIssuer(issuer);
		assertion.setConditions(createCondition(asertionConsumerURL,requestIssuer));
		
		if (authnStatement != null)
			assertion.getAuthnStatements().add(authnStatement);

		if (attributeStatement != null)
			assertion.getAttributeStatements().add(attributeStatement);

		return assertion;
	}

	private Subject createSubject(final String subjectId, final Integer samlAssertionExpirationMins, String nameIdFormat,String acs) {
		DateTime currentDate = new DateTime();
		if (samlAssertionExpirationMins != null)
			currentDate = currentDate.plusMinutes(samlAssertionExpirationMins);

		// create name element
		NameIDBuilder nameIdBuilder = new NameIDBuilder();
		NameID nameId = nameIdBuilder.buildObject();
		nameId.setValue(subjectId);
		if(nameIdFormat!=null)nameId.setFormat(nameIdFormat);

		//subject confirmation
		SubjectConfirmationDataBuilder dataBuilder = new SubjectConfirmationDataBuilder();
		SubjectConfirmationData subjectConfirmationData = dataBuilder.buildObject();
		subjectConfirmationData.setNotOnOrAfter(currentDate);
		subjectConfirmationData.setRecipient(acs);

		SubjectConfirmationBuilder subjectConfirmationBuilder = new SubjectConfirmationBuilder();
		SubjectConfirmation subjectConfirmation = subjectConfirmationBuilder.buildObject();
		subjectConfirmation.setMethod("urn:oasis:names:tc:SAML:2.0:cm:bearer");
		subjectConfirmation.setSubjectConfirmationData(subjectConfirmationData);

		// create subject element
		SubjectBuilder subjectBuilder = new SubjectBuilder();
		Subject subject = subjectBuilder.buildObject();
		subject.setNameID(nameId);
		subject.getSubjectConfirmations().add(subjectConfirmation);
		return subject;
	}

	private AuthnStatement createAuthnStatement(final DateTime issueDate) {
		// create authcontextclassref object
		AuthnContextClassRefBuilder classRefBuilder = new AuthnContextClassRefBuilder();
		AuthnContextClassRef classRef = classRefBuilder.buildObject();
		classRef.setAuthnContextClassRef("urn:oasis:names:tc:SAML:2.0:ac:classes:Password");

		// create authcontext object
		AuthnContextBuilder authContextBuilder = new AuthnContextBuilder();
		AuthnContext authnContext = authContextBuilder.buildObject();
		authnContext.setAuthnContextClassRef(classRef);

		// create authenticationstatement object
		AuthnStatementBuilder authStatementBuilder = new AuthnStatementBuilder();
		AuthnStatement authnStatement = authStatementBuilder.buildObject();
		authnStatement.setAuthnInstant(issueDate);
		authnStatement.setAuthnContext(authnContext);

		return authnStatement;
	}

	private AttributeStatement createAttributeStatement(HashMap<String, List<String>> attributes) {
		// create authenticationstatement object
		AttributeStatementBuilder attributeStatementBuilder = new AttributeStatementBuilder();
		AttributeStatement attributeStatement = attributeStatementBuilder.buildObject();

		AttributeBuilder attributeBuilder = new AttributeBuilder();
		if (attributes != null) {
			for (Map.Entry<String, List<String>> entry : attributes.entrySet()) {
				Attribute attribute = attributeBuilder.buildObject();
				attribute.setName(entry.getKey());

				for (String value : entry.getValue()) {
					XSStringBuilder stringBuilder = new XSStringBuilder();
					XSString attributeValue = stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME,
							XSString.TYPE_NAME);
					attributeValue.setValue(value);
					attribute.getAttributeValues().add(attributeValue);
				}

				attributeStatement.getAttributes().add(attribute);
			}
		}

		return attributeStatement;
	}
	
	private Conditions createCondition(String asertionConsumerURL, String requestIssuer) {
		DateTime dateTime = new DateTime();
		  /* Create and add Conditions element to assertion */
		  // Initialize ConditionsBean
		  ConditionsBean conditionsBean = new ConditionsBean();
		  DateTime aftersserTime = dateTime.plusMinutes(10);
		  conditionsBean.setNotAfter(aftersserTime);
		  conditionsBean.setNotBefore(dateTime);
		  conditionsBean.setOneTimeUse(true);
		  conditionsBean.setTokenPeriodMinutes(5);

		  // Create and add audience restriction to conditionsBean
		  List<AudienceRestrictionBean> audienceRestrictions = new ArrayList<AudienceRestrictionBean>();

		  AudienceRestrictionBean bean = new AudienceRestrictionBean();
		  bean.getAudienceURIs().add(properties.getProperty(asertionConsumerURL));
		  //bean.getAudienceURIs().add(requestIssuer);
		  audienceRestrictions.add(bean);

		  conditionsBean.setAudienceRestrictions(audienceRestrictions);

		  Conditions conditions = SAML2ComponentBuilder.createConditions(conditionsBean);
		  return conditions;
	}
	
	private Properties getProperties() {
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream reader = classLoader.getResourceAsStream("/application.properties");
			Properties properties = new Properties();
			properties.load(reader);
			return properties;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}