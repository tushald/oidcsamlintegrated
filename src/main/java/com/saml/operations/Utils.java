package com.saml.operations;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class Utils {
	static Logger logger = Logger.getLogger(Utils.class.getName());

	public static String getPostMessage(String encodedSamlResponse, String acs, String relayState) {
		logger.info("Sending response to --" + acs);

		StringBuffer postMessage = new StringBuffer();
		postMessage.append("<html>" + "<body onload='document.forms[\"saml-form\"].submit()'>"
				+ "<form name='saml-form' action='" + acs + "' method='POST'>"
				+ "<input type='hidden' name='SAMLResponse' value='" + encodedSamlResponse + "' />");
		if (relayState != null)
			postMessage.append("<input type='hidden' name='RSelayState' value='" + relayState + "'/>");

		postMessage.append("<input type='hidden' value='submit' /></body></html>");

		return postMessage.toString();
	}

	public static String encodeSaml(String samlResponse) {
		try {

			logger.info("Attempting to base64 encode the response.");

			String strFinalResponse = new String(Base64.encodeBase64(samlResponse.getBytes("UTF-8")), "UTF-8");

			strFinalResponse = strFinalResponse.trim();

			StringBuilder formattedResponse = new StringBuilder();

			for (int i = 0; i < strFinalResponse.length(); i++) {
				if (((i % 60) == 0) && i != 0)
					formattedResponse.append("\n");

				formattedResponse.append(strFinalResponse.charAt(i));
			}

			strFinalResponse = formattedResponse.toString();

			logger.info("Successfully base64 encoded saml response");
			logger.info("The base64 encoded saml response is : ");
			logger.info(strFinalResponse);
			return strFinalResponse;

		} catch (Exception e) {
			logger.error("Exception while trying to access session data .");
			logger.error("The error is : " + e.toString());
			e.printStackTrace();

		}
		return null;
	}
}
