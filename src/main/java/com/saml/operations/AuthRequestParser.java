package com.saml.operations;

import javax.servlet.http.HttpServletRequest;

import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.saml2.binding.decoding.HTTPRedirectDeflateDecoder;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLConfigurator;
import org.opensaml.xml.parse.StaticBasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuthRequestParser {
	static Logger logger = LoggerFactory.getLogger(AuthRequestParser.class.getSimpleName());
	
	private static String[] xmlToolingConfigs = { "/default-config.xml", "/schema-config.xml", "/signature-config.xml",
			"/signature-validation-config.xml", "/encryption-config.xml", "/encryption-validation-config.xml",
			"/soap11-config.xml", "/wsfed11-protocol-config.xml", "/saml1-assertion-config.xml",
			"/saml1-protocol-config.xml", "/saml1-core-validation-config.xml", "/saml2-assertion-config.xml",
			"/saml2-protocol-config.xml", "/saml2-core-validation-config.xml", "/saml1-metadata-config.xml",
			"/saml2-metadata-config.xml", "/saml2-metadata-validation-config.xml", "/saml2-metadata-attr-config.xml",
			"/saml2-metadata-idp-discovery-config.xml", "/saml2-metadata-ui-config.xml",
			"/saml2-protocol-thirdparty-config.xml", "/saml2-metadata-query-config.xml",
			"/saml2-assertion-delegation-restriction-config.xml", "/saml2-ecp-config.xml",
			"/xacml10-saml2-profile-config.xml", "/xacml11-saml2-profile-config.xml", "/xacml20-context-config.xml",
			"/xacml20-policy-config.xml", "/xacml2-saml2-profile-config.xml", "/xacml3-saml2-profile-config.xml",
			"/wsaddressing-config.xml", "/wssecurity-config.xml", };
	
	public AuthRequestParser() {
		init();
	}
	
	private void init() {
		try {
			XMLConfigurator configurator = new XMLConfigurator();
			Class<Configuration> clazz = Configuration.class;

			for (String config : xmlToolingConfigs) {
				configurator.load(clazz.getResourceAsStream(config));
			}

			StaticBasicParserPool pp = new StaticBasicParserPool();
			pp.setMaxPoolSize(50);
			pp.initialize();
			Configuration.setParserPool(pp);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (XMLParserException e) {
			e.printStackTrace();
		}
	}

	public AuthnRequest parseAuthRequest(HttpServletRequest req) {
		logger.info("parsing SAML request...");
		
		if(req.getParameter("SAMLRequest")==null) {
			logger.error("No SAML request found.");
			return null;			
		}			
		
		logger.debug("SAML reuest : "+req.getParameter("SAMLRequest"));
		
		HTTPRedirectDeflateDecoder decoder = new HTTPRedirectDeflateDecoder();
		BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject> context = new BasicSAMLMessageContext<SAMLObject, AuthnRequest, SAMLObject>();
		context.setInboundMessageTransport(new HttpServletRequestAdapter(req));
		try {
			decoder.decode(context);
		} catch (MessageDecodingException e) {
			e.printStackTrace();
		} catch (org.opensaml.xml.security.SecurityException e) {
			e.printStackTrace();
		}
		AuthnRequest authnRequest = (AuthnRequest) context.getInboundMessage();
		logger.info("Request parssing success.");
		return authnRequest;
	}

}
