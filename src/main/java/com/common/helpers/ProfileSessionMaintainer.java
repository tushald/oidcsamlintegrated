package com.common.helpers;

import java.util.HashMap;

public class ProfileSessionMaintainer {
	
	private static HashMap<String,UserProfile> sessionProfiles = new HashMap<String, UserProfile>();
	
	public static UserProfile getSessionProfile(String id) {
		return sessionProfiles.get(id);
	}
	
	public static boolean createSessionProfile(String id, UserProfile profile) {
		if (profile == sessionProfiles.put(id, profile))
			return true;
		else
			return false;
	}

}
