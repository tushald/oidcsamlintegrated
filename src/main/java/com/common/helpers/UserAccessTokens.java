package com.common.helpers;

import com.nimbusds.jwt.JWT;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;


//Class to store tokens
public class UserAccessTokens {
	
	private JWT JWToken;
	private AccessToken accessToken;
	private RefreshToken refreshToken;
	
	public JWT getJWToken() {
		return JWToken;
	}
	public void setJWToken(JWT jWToken) {
		JWToken = jWToken;
	}
	public AccessToken getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(AccessToken accessToken) {
		this.accessToken = accessToken;
	}
	public RefreshToken getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(RefreshToken refreshToken) {
		this.refreshToken = refreshToken;
	}
}
