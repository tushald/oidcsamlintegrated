package com.oidc.configure;

import java.io.InputStream;
import java.util.Properties;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimplePBEConfig;
import org.jasypt.properties.EncryptableProperties;

//class for providing config properties
public class OPConfiguration {

	private static Properties config = loadProperties();

	private static Properties loadProperties() {
		try {
			
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream reader = classLoader.getResourceAsStream("/opconfig.properties");
			Properties encryptedProperties = new Properties();
			encryptedProperties.load(reader);
			SimplePBEConfig config = new SimplePBEConfig(); 
			config.setAlgorithm("PBEWithMD5AndTripleDES");
			config.setKeyObtentionIterations(1000);
			config.setPassword("SAMLOIDCProperties");
			StandardPBEStringEncryptor encryptor = new org.jasypt.encryption.pbe.StandardPBEStringEncryptor();
			encryptor.setConfig(config);
			encryptor.initialize();
			Properties properties = new EncryptableProperties(encryptedProperties, encryptor);
			return properties;
		}

		catch (Exception e) {
			
			System.out.println("File not found");
			e.printStackTrace();
			return null;
		}
	}
	
	public static Properties getConfig() {
		return config;
	}

}
