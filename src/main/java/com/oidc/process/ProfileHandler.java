package com.oidc.process;

import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.common.helpers.UserAccessTokens;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoResponse;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.oidc.configure.OPConfiguration;

public class ProfileHandler {

	public Map<String, String> generateUserProfile(UserAccessTokens userAccessToken, String assConURI) {
		try {
			
			Properties config = OPConfiguration.getConfig();
			String domain = config.getProperty(assConURI);

			BearerAccessToken bearerToken = (BearerAccessToken) userAccessToken.getAccessToken();
			URI userInfoEndpoint = new URI(config.getProperty(domain+ ".userInfo.uri"));

			// Make the request
			HTTPResponse httpResponse = new UserInfoRequest(userInfoEndpoint, bearerToken)
					.toHTTPRequest()
					.send();

			// Parse the response
			UserInfoResponse userInfoResponse = UserInfoResponse.parse(httpResponse);

			if (! userInfoResponse.indicatesSuccess()) {
				// The request failed, e.g. due to invalid or expired token
				System.out.println(userInfoResponse.toErrorResponse().getErrorObject().getCode());
				System.out.println(userInfoResponse.toErrorResponse().getErrorObject().getDescription());
				return null;
			}

			// Extract the claims
			UserInfo userInfo = userInfoResponse.toSuccessResponse().getUserInfo();
			Iterator<String> claims = UserInfo.getStandardClaimNames().iterator();
			Map<String, String> userProfile = new HashMap<String, String>();
			
			while (claims.hasNext()) {
				String claim = (String) claims.next();
				userProfile.put(claim, userInfo.getStringClaim(claim));
			}
			
			for (Map.Entry<String, String> entry: userProfile.entrySet()) {
				System.out.println(entry.getKey() + " : " + entry.getValue());
			}
			
			/*UserProfile userProfile = new UserProfile();
			userProfile.setSubject(userInfo.getSubject().toString());
			userProfile.setName(userInfo.getName());
			userProfile.setEmail(userInfo.getEmailAddress());*/
			
			
			return userProfile;
		}
		
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
