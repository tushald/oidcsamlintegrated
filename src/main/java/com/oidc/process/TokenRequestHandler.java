package com.oidc.process;

import java.net.URI;
import java.util.Properties;

import com.common.helpers.UserAccessTokens;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.AuthorizationCodeGrant;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponseParser;
import com.oidc.configure.OPConfiguration;

public class TokenRequestHandler {

	public UserAccessTokens generateUserAccessToken(String code, String assConURI) {
		try {
			
			Properties config = OPConfiguration.getConfig();
			String domain = config.getProperty(assConURI);

			// Construct the code grant from the code obtained from the authz endpoint
			// and the original callback URI used at the authz endpoint
			AuthorizationCode authCode = new AuthorizationCode(code);
			URI callback = new URI(config.getProperty(domain + ".callback.uri"));
			AuthorizationGrant codeGrant = new AuthorizationCodeGrant(authCode, callback);

			// The credentials to authenticate the client at the token endpoint
			ClientID clientID = new ClientID(config.getProperty(domain + ".clientid"));
			Secret clientSecret = new Secret(config.getProperty(domain + ".clientSecret"));
			ClientAuthentication clientAuth = new ClientSecretBasic(clientID, clientSecret);

			// The token endpoint
			URI tokenEndpoint = new URI(config.getProperty(domain + ".endPoint.uri"));

			// Make the token request
			TokenRequest tokenRequest = new TokenRequest(tokenEndpoint, clientAuth, codeGrant);

			TokenResponse tokenResponse = OIDCTokenResponseParser.parse(tokenRequest.toHTTPRequest().send());

			if (! tokenResponse.indicatesSuccess()) {
				throw new Exception();
			}

			OIDCTokenResponse successResponse = (OIDCTokenResponse)tokenResponse.toSuccessResponse();

			// Get the ID and access token, the server may also return a refresh token
			UserAccessTokens userAuthTokens = new UserAccessTokens();
			userAuthTokens.setJWToken(successResponse.getOIDCTokens().getIDToken());
			userAuthTokens.setAccessToken(successResponse.getOIDCTokens().getAccessToken());
			userAuthTokens.setRefreshToken(successResponse.getOIDCTokens().getRefreshToken());

			return userAuthTokens;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
