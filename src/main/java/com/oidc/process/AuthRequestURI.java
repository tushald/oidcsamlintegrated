package com.oidc.process;

import java.net.URI;
import java.util.Properties;

import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import com.nimbusds.openid.connect.sdk.Nonce;
import com.oidc.configure.OPConfiguration;

public class AuthRequestURI {

	public URI generateAuthRequestURI(String assConURI) {
		try {
			
			Properties config = OPConfiguration.getConfig();
			String domain = config.getProperty(assConURI);

			// The client identifier provisioned by the server
			ClientID clientID = new ClientID(config.getProperty(domain + ".clientid"));

			// The client callback URL
			URI callback = new URI(config.getProperty(domain + ".callback.uri"));

			// Generate random state string for pairing the response to the request
			State state = new State();

			// Generate nonce
			Nonce nonce = new Nonce();

			// Compose the request (in code flow)
			AuthenticationRequest authRequest = new AuthenticationRequest(
					new URI(config.getProperty(domain + ".auth.uri")),
					new ResponseType(config.getProperty(domain + ".respType")),
					Scope.parse(config.getProperty(domain + ".scope")),
					clientID,
					callback,
					state,
					nonce);
			URI uri = authRequest.toURI();

			return uri;
			
		}
		
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
