package com.oidc.controller;

import java.io.IOException;
import java.net.URI;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.opensaml.saml2.core.AuthnRequest;

import com.oidc.process.AuthRequestURI;


//Servlet to check session, if session doesnt exists redirect user to Authentication server
public class AuthenticationRequester extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {

			HttpSession session = request.getSession();

			if ((session.getAttribute("userProfile")) == null) {

				//get domain for request handling
				AuthnRequest authRequest = (AuthnRequest)session.getAttribute("authnRequest");
				String assConURI = authRequest.getAssertionConsumerServiceURL();
				
				
				//generate uri for redirection and redirect user
				AuthRequestURI authReqURI = new AuthRequestURI();
				URI requestURI = authReqURI.generateAuthRequestURI(assConURI);

				response.sendRedirect(requestURI.toString());

			}

			else {
				//forward control with Users Profile
				RequestDispatcher dispatcher = request.getRequestDispatcher("profileDisplay");
				dispatcher.forward(request, response);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);

	}

}
