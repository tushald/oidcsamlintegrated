package com.oidc.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.opensaml.saml2.core.AuthnRequest;

import com.common.helpers.UserAccessTokens;
import com.oidc.process.ProfileHandler;

//servlet to get user profile using access token form endpoint
public class ProfileRequester extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			//get user op domain
			HttpSession session = request.getSession();
			AuthnRequest authRequest = (AuthnRequest)session.getAttribute("authnRequest");
			String assConURI = authRequest.getAssertionConsumerServiceURL();
			
			//get user access token from request attributes and request for user profile from endpoint 
			ProfileHandler profile = new ProfileHandler();
			Map<String, String> userProfile = profile.generateUserProfile((UserAccessTokens)request.getAttribute("userAccessToken"), assConURI);
			
			//store user profile in session object
			(request.getSession()).setAttribute("userProfile", userProfile);
			
			//store user profile in request attributes and forward
			//request.setAttribute("userProfile", userProfile);
			
			//forward control with Users Profile
			RequestDispatcher dispatcher = request.getRequestDispatcher("saml/asserter");
			dispatcher.forward(request, response);
			
		}
		
		catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
	}

}
