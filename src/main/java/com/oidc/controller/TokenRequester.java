package com.oidc.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.opensaml.saml2.core.AuthnRequest;

import com.common.helpers.UserAccessTokens;
import com.oidc.process.TokenRequestHandler;

//servlet to get access token using authentication code
public class TokenRequester extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			
			//get user op domain
			HttpSession session = request.getSession();
			AuthnRequest authRequest = (AuthnRequest)session.getAttribute("authnRequest");
			String assConURI = authRequest.getAssertionConsumerServiceURL();
			
			TokenRequestHandler tokenRequest = new TokenRequestHandler();
			
			// Get the ID and access token, the server may also return a refresh token
			UserAccessTokens userAuthTokens = tokenRequest.generateUserAccessToken(request.getParameter("code"), assConURI);
			
			//forward control with Tokens
			request.setAttribute("userAccessToken", userAuthTokens);
			System.out.println(userAuthTokens);
			RequestDispatcher dispatcher = request.getRequestDispatcher("profileRequester");
			dispatcher.forward(request, response);
			
		}
		
		catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("error.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
